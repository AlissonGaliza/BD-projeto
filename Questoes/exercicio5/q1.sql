-- Mostre o nome do funcionário e o nome do setor onde está alocado

use mydb;

select setor from funcionario;
select codigo_id from setor;

select funcionario.nome,
	setor.nome
from funcionario
inner join setor on funcionario.setor=setor.codigo_id;


