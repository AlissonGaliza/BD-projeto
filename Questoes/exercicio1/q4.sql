use mydb;

/* Mostre uma coluna adicional na tabela de produtos, informando a diferença entre o preço de venda e o preço
de custo.*/



-- criar a coluna
-- alter table produto add diferenca numeric(16,2) null after venda;
-- alter table produto drop column diferenca;

-- inserir os valores
 update produto set diferenca = (produto.venda - produto.custo) where produto.codigo_id > 0;

-- apenas mostrar o resultado. Não fica salvo
-- select *,(produto.venda-produto.custo) as diferenca from produto;

-- verificar se deu certo
select * from produto;