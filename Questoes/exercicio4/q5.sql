-- Mostre, para cada produto, a quantidade pedidos onde ele aparece,
-- a média de preços de venda dos produtos e a média de quantidade
-- dos produtos

use mydb;

select * from itens_pedido;

select count(pedido_id) as 'Pedido', produto_id
from itens_pedido
group by produto_id;

select count(produto_id) as 'produto', produto_id
from itens_pedido
group by produto_id;

-- Resultado
select produto_id,count(*) as 'Quantidade de pedidos',
	avg(total) as 'Media do preco', 
	avg(quant) as 'Media de quant'
from itens_pedido
group by produto_id;