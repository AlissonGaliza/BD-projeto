-- Mostre a quantidade de homens e mulheres por setor

use mydb;

select sexo, setor, count(codigo_id) as 'Quantidade'
from funcionario
group by sexo, setor;

select * from funcionario;