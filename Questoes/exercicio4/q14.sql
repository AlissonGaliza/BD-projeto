-- Mostre a quantidade de funcionários nascidos em cada cidade
-- existente na tabela de funcionários,
-- agrupando os nascimentos por décadas, produzindo o seguinte
-- resultado:


-- Decada Funcionarios
-- --------------- -----------------
-- 40 8
-- 50 13
-- 60 11
-- 70 45

use mydb;

select  (floor(YEAR(mydb.funcionario.data_nascimento)/10))*10 as 'Decada',
 	count(codigo_id) as 'Funcionarios'
from funcionario
group by naturalidade,  floor(YEAR(mydb.funcionario.data_nascimento)/10)*10;



