-- Mostre a média de idade em anos dos funcionários por setor 
-- e por sexo
use mydb;

select * from funcionario;

-- resposta
select avg(year(current_date())-year(data_nascimento)) 
as 'Media de idade', setor, sexo 
from funcionario
group by setor,sexo;

select avg(datediff(current_date(),data_nascimento)) 
as 'Media de idade', setor, sexo 
from mydb.funcionario
group by setor,sexo;
