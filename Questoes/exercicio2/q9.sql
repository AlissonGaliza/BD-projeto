-- Qual o nome e o endereço dos funcionários que nasceram no mês atual ?
use mydb;


select nome, tipo_logradouro, logradouro, complemento, bairro, cidade, cep
from funcionario  WHERE MONTH(data_nascimento) = MONTH(CURRENT_DATE());