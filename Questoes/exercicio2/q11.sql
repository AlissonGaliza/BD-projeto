-- Mostre o nome, o estoque real e o tipo dos produtos 
-- cujo estoque real está inferior ao estoque mínimo

-- inserindo um resultado
insert into mydb.produto (nome, tipo, unidade, estoque_real, estoque_virtual, estoque_minimo, custo, venda)
values ('Pneu Aro caralho a 4', 5, 'und', 40, 33, 42, 190, 350);

select produto.nome, mydb.produto.estoque_real, mydb.produto.tipo
from mydb.produto
where mydb.produto.estoque_real < mydb.produto.estoque_minimo;



