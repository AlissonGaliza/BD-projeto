-- Mostre o nome e o preço de venda dos produtos
-- cujo nome contém a palavra “FREIO”

-- inserindo um resultado
insert into mydb.produto (nome, tipo, unidade, estoque_real, estoque_virtual, estoque_minimo, custo, venda)
values ('Pneu FREIO caralho a 4', 5, 'und', 40, 33, 42, 190, 350),
		('FREIO Pneu caralho a 4', 5, 'und', 40, 33, 42, 190, 350),
		('Pneu caralho a 4 FREIO', 5, 'und', 40, 33, 42, 190, 350);

-- verificando
select * from mydb.produto;

-- buscando
select nome, custo from mydb.produto
where produto.nome regexp 'FREIO';
-- ou
-- where produto.nome like '%FREIO%';

-- uso do regexp
-- ^ inicio de string
-- $ fim de string

