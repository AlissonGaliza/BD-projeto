
use mydb2;

SELECT pedido.total, (pedido.total / (SELECT sum(pedido.total) 
from pedido) * 100) 
as 'porcentagem' FROM pedido
ORDER BY total desc;