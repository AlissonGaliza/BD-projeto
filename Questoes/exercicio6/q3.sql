-- Mostre o ranking dos produtos (nome do produto, total em R$),
-- informando a sua fatia em percentual
-- sobre do valor total vendido, mas considerando apenas os pedidos do ano
-- passado

use mydb2;


SELECT pedido.total, (pedido.total / (SELECT sum(pedido.total) 
from pedido) * 100) 
as 'porcentagem' FROM pedido
ORDER BY total desc;



-- [INCOMPLETA]
select produto.nome , produto.total, 
(produto.total/(SELECT sum(produto.total) from produto) * 100) as porcentagem
from pedido 
where datetime()
order by produto.total desc; 

