/*Caso o maior pedido feito (considerando o valor) tenha sido
feito por uma funcionária, mostre a lista de funcionárias do setor
onde ela trabalha. Caso o pedido tenha sido feito por um homem,
mostre os homens do setor, ao inves das mulheres.*/

use mydb2;

select * from funcionario;

-- ultimo funcionario
select * from funcionario
order by codigo_id desc
limit 1;


-- teste subquerey
select funcionario.sexo from funcionario
order by funcionario.codigo_id desc
limit 1;

-- forma1
select * from funcionario
where setor = (select funcionario.setor from pedido
	inner join funcionario on pedido.vendedor = funcionario.codigo_id
	where pedido.total = (select max(total) from pedido))
and sexo = (select funcionario.sexo from pedido inner join funcionario on pedido.vendedor = funcionario.codigo_id
where pedido.total = (select max(total) from pedido));


-- maneira2
select * from funcionario inner join
(select funcionario.setor, funcionario.sexo from pedido inner join funcionario on pedido.vendedor = funcionario.codigo_id
where pedido.total = (select max(total) from pedido))
as qualquer_coisa
where funcionario.sexo = qualquer_coisa.sexo
and funcionario.setor = qualquer_coisa.setor;

select if( 
	((select funcionario.sexo from funcionario
	order by funcionario.codigo_id desc
	limit 1) = 'F'),
	
    select * from fucionario where funcionario.sexo = 'F',
    'M'
) as 'condição';

select 'm';

select if( 
	((select funcionario.sexo from funcionario
	order by funcionario.codigo_id desc
	limit 1) = 'F'),
	
    funcionario.sexo = 'F',
    funcionario.sexo = 'M'
) as 'condição';
