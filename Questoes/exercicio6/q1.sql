-- Mostre a lista de pedidos pendentes com as seguintes informações:
-- código do pedido, nome do cliente, situação do pedido.
-- Mas mostre apenas aqueles pedidos dos clientes estrangeiros.

use mydb2;


select * from pedido;
select * from cliente;
select * from cidade;

select pedido.codigo_id, clientes.contato, pedido.situacao
from pedido
inner join (select cliente.contato, cliente.codigo_id from cliente 
inner join cidade on cliente.cidade = cidade.codigo_id where cidade.pais != 'BR')
 as clientes
on pedido.cliente = clientes.codigo_id
where pedido.situacao != 'T';




/*
SELECT pedido.codigo_id, cliente.contato, pedido.situacao 
FROM pedido INNER JOIN 
(SELECT codigo_id, contato FROM cliente WHERE pais != 'br') as cliente 
on pedido.cliente = cliente.codigo_id
WHERE situacao = 'P';
    
*/

    
