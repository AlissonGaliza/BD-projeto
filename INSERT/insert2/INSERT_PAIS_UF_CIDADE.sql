USE mydb2;

INSERT INTO pais(sigla_id, nome)
     VALUES ('BR', 'Brasil'),
            ('US', 'Estados Unidos'),
            ('AR', 'Argentina');

INSERT INTO uf(pais_id, sigla_id, nome)
     VALUES ('BR', 'PB', 'Paraiba'),
            ('BR', 'PE', 'Pernambuco'),
            ('AR', 'SL', 'San Luis'),
            ('AR', 'SF', 'Santa Fe'),
            ('BR', 'AC', 'Acre'),
            ('BR', 'RS', 'Rio Grande do Sul'),
            ('BR', 'RN', 'Rio Grande do Norte'),
            ('BR', 'SP', 'Sao Paulo'),
            ('AR', 'BA', 'Provincia de Buenos Aires'),
            ('US', 'CA', 'California'),
            ('US', 'AK', 'Alaska'),
            ('US', 'KS', 'Kansas');

INSERT INTO cidade(nome, uf, pais)
     VALUES ('La Plata', 'BA', 'AR'),
            ('Joao Pessoa', 'PB', 'BR'),
            ('Patos', 'PB', 'BR'),
            ('Campina Grande', 'PB', 'BR'),
            ('Sape', 'PB', 'BR'),
            ('Recife', 'PE', 'BR'),
            ('Buenos Aires', 'BA', 'AR'),
            ('Rio Branco', 'AC', 'BR'),
            ('Cruzeiro do Sul', 'AC', 'BR'),
            ('Porto Acre', 'AC', 'BR'),
            ('Sao Paulo', 'SP', 'BR'),
            ('Santa Rita', 'PB', 'BR'),
            ('Guarabira', 'PB', 'BR'),
            ('Santos', 'SP', 'BR'),
            ('Los Angeles', 'CA', 'US'),
            ('Anchorage', 'AK', 'US'),
            ('Topeka', 'KS', 'US');

