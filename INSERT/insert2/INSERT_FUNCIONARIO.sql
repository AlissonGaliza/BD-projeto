USE mydb2;

INSERT INTO funcionario(nome, sexo, setor, ramal_individual, estado_civil, data_nascimento, rg_numero,
                        naturalidade, cpf, sangue_fator, sangue_rh, tipo_logradouro, logradouro,
                        complemento, bairro, cidade, cep, fone, funcao, admissao, email, salario)
     VALUES ('Silvino Silvano Silveira', 'M', 1, '0800', 'S', '1964-04-18 10:34:09',
            '6258003', 3, '78945612538', 'AB', '+', 'RUA', 'Rua das Moitas Ardentes',
            'Casa', 'Centro', 4, '58200000', '08002148965237', 2, '2015-06-18 10:34:09',
            'cobra@ig.com',3500),

            ('Cristiane Bloco das Virgens', 'F',2,'5863','C','1983-06-18 10:34:09','6258600',
            2,'98675342805','A','-','AVE', 'Av. Pedro Cristina Motta','Apartamento Sagaz',
            'Florentina de Jesus',3,'10867642','85741968500234',1,'2014-06-18 10:34:09',
		    'vigimainha@mobil.net',7812),

    		('Falácio Grande da Silva','M', 3, '6912','C', '1992-06-18 10:34:09','1248600',
            1, '98673812805', 'O', '-','RUA', 'Rua Joventina Jovêncio Jovem', 'Casa','Cristo',
            2,'62786004','8395741285',3,'2012-06-18 10:34:09','oxente@nex.com',15236 ),

		    ('Marcelo Teixeira','M', 3, '6911','S', '1991-11-17 10:34:09','1248601',12,
		    '12373814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcelo@nex.com',888 ),

		    ('Fernanda Marques','M', 3, '6910','C', '1989-08-13 10:34:09','1248602',13,
		    '95678812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','fernanda@nex.com',8136 ),

		    ('Marcela Leite','F', 2, '6909','S', '1991-11-17 10:34:09','121',12,
		    '13333814505', 'A', '+','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marcela@nex.com',1000 ),

		    ('Fernandinho Beira Mar','M', 2, '6908','D', '1970-04-13 10:34:09','122',13,
		    '97778812875', 'O', '+','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','fernandinho@nex.com',1200 ),

		    ('Marceleto Lopes','M', 2, '6907','D', '1999-04-17 10:34:09','123',12,
		    '15553814505', 'A', '-','AVE', 'Av Laura Costa', 'Casa','Bairro Santo',12,
		    '62786098','8395796385',3,'2012-06-18 10:34:09','marceleto@nex.com',2000 ),

		    ('Jaqueline Sousa','F', 3, '6906','C', '1945-05-13 10:34:09','124',13,
		    '94448812875', 'O', '-','RUA', 'Rua Louca da Silva', 'Casa','Bairro da Paz',13,
		    '62746804','8396661285',3,'2012-06-18 10:34:09','jaq@nex.com',6000 );

SELECT * FROM funcionario;