SELECT f.nome, f.logradouro, f.complemento, f.bairro, c.nome as cidade, uf.nome as uf, p.nome as pais
  FROM funcionario as f, cidade as c, uf, pais as p
 WHERE datepart(MONTH, data_nascimento) = datepart(MONTH, getdate())
   AND c.codigo_id = f.cidade
   AND uf.sigla_id = c.uf
   AND c.pais = uf.pais_id
   AND p.sigla_id = uf.pais_id;
   
