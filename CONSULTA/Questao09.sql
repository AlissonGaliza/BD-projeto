  SELECT p.codigo_id as pedido, c.razao_social as cliente, f.nome AS vendedor
    FROM pedido AS p, cliente AS c, funcionario AS f
   WHERE p.cliente = c.codigo_id
     AND p.vendedor = f.codigo_id 
ORDER BY p.codigo_id;
