UPDATE produto
   SET produto.venda = 1.1 * produto.venda
  FROM tipo_produto, produto
 WHERE produto.tipo = tipo_produto.codigo_id
   AND tipo_produto.nome = '%MATERIA PRIMA RECICLADA%';
