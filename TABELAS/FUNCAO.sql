USE mydb;

CREATE TABLE funcao
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(50) NULL,
    salario   NUMERIC(16,2)    NULL,

    CONSTRAINT pk_funcao_codigo PRIMARY KEY(codigo_id)
);
