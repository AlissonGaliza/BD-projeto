USE mydb;

CREATE TABLE setor
(
    codigo_id   SMALLINT AUTO_INCREMENT,
    nome        CHAR(50) NULL,
    sigla       CHAR(10) NULL,
    ramal       CHAR(3)  NULL,
    superior    SMALLINT NULL,
    responsavel INT      NULL,

    CONSTRAINT pk_setor_codigo      PRIMARY KEY(codigo_id),
    CONSTRAINT fk_setor_responsavel FOREIGN KEY(responsavel) REFERENCES funcionario(codigo_id),
    CONSTRAINT fk_setor_superior    FOREIGN KEY(superior) REFERENCES setor(codigo_id)
);
