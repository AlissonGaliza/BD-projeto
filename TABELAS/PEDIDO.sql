USE mydb;

CREATE TABLE pedido
(
    codigo_id   INT           AUTO_INCREMENT,
    cliente     SMALLINT      NOT NULL,
    data_pedido DATETIME NOT NULL,
    total       NUMERIC(16,2)         NULL,
    situacao    CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),
    vendedor    INT           NULL,

    CONSTRAINT pk_pedido_codigo   PRIMARY KEY(codigo_id),
    CONSTRAINT fk_pedido_cliente  FOREIGN KEY(cliente)  REFERENCES cliente(codigo_id),
    CONSTRAINT fk_pedido_vendedor FOREIGN KEY(vendedor) REFERENCES funcionario(codigo_id)
);
