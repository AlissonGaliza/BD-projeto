USE mydb;

CREATE TABLE funcionario
(
    codigo_id        INT           AUTO_INCREMENT,
    nome             CHAR(30)      NOT NULL,
    sexo             CHAR(1)       NOT NULL
        CHECK (sexo = 'M' OR sexo = 'F'),
    setor            SMALLINT      NOT NULL,
    ramal_individual CHAR(4)       NOT NULL,
    estado_civil     CHAR(1)       NOT NULL
        CHECK (estado_civil = 'S' OR estado_civil = 'C' OR estado_civil = 'D'
               OR estado_civil = 'V' OR estado_civil = 'O'),
    data_nascimento  DATETIME NOT NULL,
    rg_numero        CHAR(15)      NULL,
    /*nacionalidade    CHAR(2)       NOT NULL,*/
    naturalidade     INT           NOT NULL,
    cpf              CHAR(11)      NULL,
    sangue_fator     CHAR(2)       NULL,
    sangue_rh        CHAR(1)       NULL,
    tipo_logradouro  CHAR(3)       NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro       CHAR(30)      NOT NULL,
    complemento      CHAR(30)      NULL,
    bairro           CHAR(20)      NOT NULL,
    cidade           INT           NOT NULL,
    cep              CHAR(8)       NULL,
    fone             CHAR(14)      NULL,
    funcao           INT           NOT NULL,
    admissao         DATETIME      NOT NULL,
    email            CHAR(40)      NULL,
    salario          NUMERIC(16,2)         NOT NULL,

    CONSTRAINT pk_funcionario_codigo       PRIMARY KEY(codigo_id),
    CONSTRAINT fk_funcionario_naturalidade FOREIGN KEY(naturalidade) REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_cidade       FOREIGN KEY(cidade)       REFERENCES cidade(codigo_id),
    CONSTRAINT fk_funcionario_funcao       FOREIGN KEY(funcao)       REFERENCES funcao(codigo_id),
    CONSTRAINT cpf_unique                  UNIQUE(cpf),
    CONSTRAINT rg_unique                   UNIQUE(rg_numero)
);
