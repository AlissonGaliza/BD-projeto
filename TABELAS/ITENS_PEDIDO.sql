USE mydb;

CREATE TABLE itens_pedido
(
    pedido_id  INT           NOT NULL,
    produto_id SMALLINT      NOT NULL,
    quant      NUMERIC(10,3) NOT NULL,
    total      NUMERIC(16,2) NULL,
    situacao   CHAR(1)       NULL
        CHECK (situacao = 'A' OR situacao = 'P' OR situacao = 'T'),

    CONSTRAINT pk_itens_pedido         PRIMARY KEY(pedido_id, produto_id),
    CONSTRAINT fk_itens_pedido_pedido  FOREIGN KEY(pedido_id)  REFERENCES pedido(codigo_id),
    CONSTRAINT fk_itens_pedido_produto FOREIGN KEY(produto_id) REFERENCES produto(codigo_id)
);
