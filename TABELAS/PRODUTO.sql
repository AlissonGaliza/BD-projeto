USE mydb;

CREATE TABLE produto
(
    codigo_id       SMALLINT              AUTO_INCREMENT,
    nome            CHAR(40)              NOT NULL,
    tipo            SMALLINT              NOT NULL,
    unidade         CHAR(3)               NOT NULL,
    estoque_real    NUMERIC(16,3)         NULL,
    estoque_virtual NUMERIC(16,3)         NULL,
    estoque_minimo  NUMERIC(16,3)         NULL,
    custo           NUMERIC(16,2)         NULL,
    venda           NUMERIC(16,2)         NULL,

    CONSTRAINT pk_produto_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_produto_tipo   FOREIGN KEY(tipo) REFERENCES tipo_produto(codigo_id)
);
