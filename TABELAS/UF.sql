USE mydb;

CREATE TABLE uf
(
    sigla_id CHAR(2)  NOT NULL,
    pais_id  CHAR(2)  NOT NULL,
    nome     CHAR(30) NOT NULL,

    CONSTRAINT pk_uf_sigla PRIMARY KEY(sigla_id, pais_id),
    CONSTRAINT fk_uf_pais  FOREIGN KEY(pais_id) REFERENCES pais(sigla_id)
);
