USE mydb;

CREATE TABLE cidade
(
    codigo_id INT      AUTO_INCREMENT,
    nome      CHAR(40) NULL,
    uf        CHAR(2)  NOT NULL,
    pais      CHAR(2)  NOT NULL,

    CONSTRAINT pk_cidade_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT fk_cidade_uf     FOREIGN KEY(uf, pais) REFERENCES uf(sigla_id, pais_id),
    CONSTRAINT fk_cidade_pais   FOREIGN KEY(pais) REFERENCES pais(sigla_id)
);
