USE mydb;

CREATE TABLE pais
(
    sigla_id CHAR(2)  NOT NULL,
    nome     CHAR(40) NULL,

    CONSTRAINT pk_pais_sigla PRIMARY KEY(sigla_id)
);
