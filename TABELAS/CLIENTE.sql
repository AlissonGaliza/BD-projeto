USE mydb;

CREATE TABLE cliente
(
    codigo_id       SMALLINT AUTO_INCREMENT,
    cgc_cpf         CHAR(15)               NOT NULL,
    tipo            CHAR(1)                NOT NULL
        CHECK (tipo = 'F' or tipo = 'J'),
    razao_social    CHAR(30)               NOT NULL,
    tipo_logradouro CHAR(3)                NOT NULL
        CHECK (tipo_logradouro = 'AVE' OR tipo_logradouro = 'RUA' OR tipo_logradouro = 'PRA' OR tipo_logradouro = 'TRA'
               OR tipo_logradouro = 'TRA' OR tipo_logradouro = 'ROD' OR tipo_logradouro = 'VIL'),
    logradouro      CHAR(30)               NOT NULL,
    complemento     CHAR(20)               NULL,
    bairro          CHAR(20)               NOT NULL,
    cidade          INT                    NOT NULL,
    cep             CHAR(8)                NOT NULL,
    fone            CHAR(14)               NOT NULL,
    contato         CHAR(30)               NOT NULL,
    fax             CHAR(14)               NULL,
    insc_est        CHAR(16)               NULL,
    email           CHAR(40)               NULL,
    obs             TEXT                   NULL,

    CONSTRAINT pk_cliente_codigo PRIMARY KEY(codigo_id),
    CONSTRAINT cgc_cpf_unique    UNIQUE(cgc_cpf),
    CONSTRAINT fk_cliente_cidade FOREIGN KEY(cidade)   REFERENCES cidade(codigo_id)
);
