USE mydb;

CREATE TABLE tipo_produto
(
    codigo_id SMALLINT AUTO_INCREMENT,
    nome      CHAR(50) NULL,

    CONSTRAINT pk_tipo_produto_codigo PRIMARY KEY(codigo_id)
);
